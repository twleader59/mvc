<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.12.3.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>AJAX</title>

<style type="text/css">
	.button {
		background-color: #7CA05F;
	    border: none;
	    color: white;
	    padding: 15px 32px;
	    text-align: center;
	    text-decoration: none;
	    display: inline-block;
	    font-size: 16px;
	    margin: 4px 2px;
	    cursor: pointer;
	}
</style>
	
<script type="text/javascript">
$(function() {
	$('#html-button .button').bind('click', (function() {
		$("#article").load('../html/bumblerMa.html');
	}));

	$('#json-button .button').bind('click', (function() {
		$.getJSON('../json/president.json', function(data) {
			$('#article').empty();
			$.each(data, function(entryIdx, entry) {
				var html = '';
				html += '<H1>' + entry['title'] + '</H1>';
				html += '<H3>' + entry['sub-title'] + '</H3>';
				html += '<DIV>' + entry['date'] + '</DIV>';
				html += '<br/>' + entry['content'] + '</br>';
				$('#article').append(html);
			}); 
		});
	}));
});
</script>

</head>
<body>

<div id="article"></div>
<div id="html-button">
	<input type="button" class="button" value="html"/>
</div>

<div id="json-button">
	<input type="button" class="button" value="json"/>
</div>


</body>
</html>