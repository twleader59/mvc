<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.12.3.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>查詢股價</title>

<script type="text/javascript">
var message;

$(function() {
	$("#code").bind("keyup", function(e) {
		if ($("#code").val().length != 4) {
			return;
		}

		var myUrl = "<%=request.getContextPath()%>/searchStock.do?code=" + $('#code').val();
		
		$.ajax({
			type: 'get',
			url: myUrl,
			dataType: 'json',
			success: function(data, result) {
				if (data.responseMsg.status == "success") {
					message = $.parseJSON(data.jsondata);
					$('#name').html('股票名稱: ' + message.name);
					$('#price').html('股票價格: ' + message.price);
				}
				else {
					alert('fail');
					alert(data.responseMsg.message);
				}
			},
			error: function() {
				alert('error');
			}
		});
	});
});
</script>

</head>
<body>

<form>
	股票代號: <input id="code" name="code" type="text"/><br/>
	<div id="name"></div>
	<div id="price"></div>
</form>

</body>
</html>
