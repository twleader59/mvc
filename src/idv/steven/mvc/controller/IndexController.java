package idv.steven.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import idv.steven.mvc.form.IndexForm;

@Controller
public class IndexController {
	
	@RequestMapping("/index")
	public String login(IndexForm form) {
		if ("steven".equalsIgnoreCase(form.getUserName()) && "123456".equals(form.getPassword())) {
			
			return "success";
		}
		
		return "fail";
	}
}
