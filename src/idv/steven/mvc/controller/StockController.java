package idv.steven.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import idv.steven.mvc.form.ResponseMessage;
import idv.steven.mvc.form.StockForm;
import idv.steven.mvc.form.Stocks;

@Controller
public class StockController {
	@Autowired
	private Stocks allStocks;
	
	@RequestMapping(value="/searchStock", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getStockPrice(String code) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Map<String, StockForm> stocks = allStocks.getStocks();
		StockForm stock = stocks.get(code);
		
		if (stock != null) {
			result.put("jsondata", toJson(stock));
			result.put("responseMsg", new ResponseMessage("取得股票資料", "success"));
		}
		
		return result;
	}
	
	public String toJson(Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(object);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
