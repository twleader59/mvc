package idv.steven.mvc.form;

import java.util.Map;

public class Stocks {
	private Map<String, StockForm> stocks;

	public Map<String, StockForm> getStocks() {
		return stocks;
	}

	public void setStocks(Map<String, StockForm> stocks) {
		this.stocks = stocks;
	}
}
